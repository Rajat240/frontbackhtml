@extends('app')
@section('content')

 

<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                 @if (count($errors) > 0)					
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h4 class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error!</h4>
            <ul class="mb-0 px-0 list-style-none">
                @foreach ($errors->all() as $error)
                <li><i class="fa fa-chevron-right"></i> {{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(Session::has('flash_message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <h4 class="text-success mb-0"><i class="fa fa-check-circle"></i> {{ Session::get('flash_message') }} </h4>
        </div>
        @endif
                <div class="login_page">
                    {!! Form::open(array('url'=>'login','id'=>'login','class'=>'padding-15','role','enctype' => 'multipart/form-data' )) !!}
                        <div class="form-group">
                            <label>Enter your username</label>
                            <input type="text" class="form-control fome_career" id="uname" name="uname">
                        </div>
                        <div class="form-group">
                            <label>Enter your password</label>
                            <input type="password" name="password" class="form-control fome_career" id="exampleInputPassword1">
                        </div>
                        <div class="form-group form-check"> 
                            <input class="form-check-input fome_career" type="checkbox" name="remember"> Remember me  
                        </div>
                    
                    <p><a href="{{ URL::to('forgot_password') }}">Forgot username or password?</a></p>
                    <p><a href="{{ URL::to('signup') }}">Sign Up</a></p>
                    <button type="submit" class="btn btn_login">Login</button>
                     
                    {!! Form::close() !!}
                   
                </div>
            </div>
             
        </div>	
    </div>
</section>

@endsection